package models

type ResponseMessage struct {
	Status  int         `json:"status"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"result,omitempty"`
}
