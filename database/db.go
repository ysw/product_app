package database

import (
	"fmt"
	"log"
	"product-app/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	host     = "localhost"
	user     = "root"
	password = "password"
	dbPort   = "3306" // Default port for MySQL
	dbname   = "db_hacktive"
	db       *gorm.DB
	err      error
)

func StartDB() *gorm.DB {
	// Configure connection string for MySQL
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true", user, password, host, dbPort, dbname)

	// Open connection to MySQL database
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("Error connecting to database:", err)
	}

	fmt.Println("Successfully connected to database")

	// Migrate models to tables (equivalent to AutoMigrate)
	db.AutoMigrate(&models.User{}, &models.Product{})

	return db
}

func GetDB() *gorm.DB {
	return db
}
