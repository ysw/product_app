package controllers

import (
	"net/http"
	"product-app/helpers"
	"product-app/models"
)

func CreateResponse(err error, code int, message string, payload interface{}) (status int, response models.ResponseMessage) {

	if err != nil {
		// Handle the case where there is an error
		status = http.StatusBadRequest
		response.Status = helpers.FailedStatus
		response.Message = err.Error()
		response.Data = nil
		return
	}

	// Handle the case where there is no error
	status = helpers.HTTP_STATUS_MAP[code]
	response.Status = helpers.SuccessStatus
	response.Message = message
	response.Data = payload

	return
}
