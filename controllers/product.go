package controllers

import (
	"product-app/helpers"
	"product-app/models"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// @Summary Add Product
// @Description Add a new product
// @Tags Products
// @Accept json
// @Produce json
// @Param Authorization header string true "JWT Token"
// @Param product body models.Product true "Product data"
// @Success 200 {object} models.Product
// @Router /product [post]
func (cr Controller) CreateProduct(c *gin.Context) {

	userData := c.MustGet("userData").(jwt.MapClaims)
	contentType := helpers.GetContentType(c)
	_, _ = cr.Db, contentType
	Products := models.Product{}
	userID := uint(userData["id"].(float64))
	if contentType == appJSON {
		c.ShouldBindJSON(&Products)
	} else {
		c.ShouldBind(&Products)
	}
	Products.UserID = userID

	Result := map[string]interface{}{}

	err := cr.Db.Raw(
		"INSERT into products (title, caption, photo_url, harga, user_id, created_at) VALUES(?,?,?,?,?,?) Returning id,title,photo_url, harga, user_id, created_at, caption",
		Products.Title, Products.Caption, Products.PhotoUrl, Products.Harga, Products.UserID, time.Now(),
	).Scan(&Result).Error

	code, response := CreateResponse(err, helpers.DataCreated, helpers.SuccessMessage, Result)

	c.JSON(code, response)

}

func (cr Controller) UpdateProduct(c *gin.Context) {

	userData := c.MustGet("userData").(jwt.MapClaims)
	contentType := helpers.GetContentType(c)
	_, _ = cr.Db, contentType
	Products := models.Product{}
	produkId, _ := strconv.Atoi(c.Param("produkId"))
	userID := uint(userData["id"].(float64))
	if contentType == appJSON {
		c.ShouldBindJSON(&Products)
	} else {
		c.ShouldBind(&Products)
	}
	Products.UserID = userID

	Result := map[string]interface{}{}
	SqlStatement := "Update products SET title = ?, caption = ?, photo_url = ?, harga = ?, updated_at = ? WHERE id = ? RETURNING id, title, caption, photo_url, harga, updated_at"
	err := cr.Db.Raw(
		SqlStatement,
		Products.Title, Products.Caption, Products.PhotoUrl, Products.Harga, time.Now(), uint(produkId),
	).Scan(&Result).Error

	code, response := CreateResponse(err, helpers.DataUpdated, helpers.SuccessMessage, Result)

	c.JSON(code, response)

}

func (cr Controller) GetAllProduct(c *gin.Context) {

	Products := []models.Product{}
	err := cr.Db.Find(&Products).Error
	payload := gin.H{"products": Products}
	code, response := CreateResponse(err, helpers.SuccessStatus, helpers.SuccessMessage, payload)

	c.JSON(code, response)
}

func (cr Controller) GetOneProduct(c *gin.Context) {

	produkId, _ := strconv.Atoi(c.Param("produkId"))
	Products := models.Product{}
	err := cr.Db.Preload("User").Find(&Products, uint(produkId)).Error
	payload := gin.H{"products": Products}
	code, response := CreateResponse(err, helpers.SuccessStatus, helpers.SuccessMessage, payload)

	c.JSON(code, response)

}

func (cr Controller) DeleteProduct(c *gin.Context) {

	produkId, _ := strconv.Atoi(c.Param("produkId"))
	Products := models.Product{}
	err := cr.Db.Delete(Products, uint(produkId)).Error

	payload := gin.H{"products": Products}
	code, response := CreateResponse(err, helpers.SuccessStatus, "Your product has been successfully deleted", payload)

	c.JSON(code, response)

}
