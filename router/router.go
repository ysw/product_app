package router

import (
	"product-app/controllers"
	"product-app/middlewares"

	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
)

type Router struct {
	route *controllers.Controller
}

func NewRouter(route *controllers.Controller) *Router {
	return &Router{route: route}
}

func (rs Router) StartApp() *gin.Engine {
	r := gin.New()

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	userRouter := r.Group("/users")
	{
		userRouter.POST("/register", rs.route.UserRegister)
		userRouter.POST("/login", rs.route.UserLogin)
	}

	productRouter := r.Group("/product")
	{
		productRouter.Use(middlewares.Authentication())

		productRouter.GET("/", middlewares.ProductAuthorization(), rs.route.GetAllProduct)
		productRouter.GET("/:produkId", middlewares.ProductAuthorization(), rs.route.GetOneProduct)
		productRouter.POST("/", middlewares.ProductAuthorization(), rs.route.CreateProduct)
		productRouter.PUT("/:produkId", middlewares.ProductAuthorization(), rs.route.UpdateProduct)
		productRouter.DELETE("/:produkId", middlewares.ProductAuthorization(), rs.route.DeleteProduct)
	}

	return r
}
