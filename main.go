package main

import (
	"product-app/controllers"
	"product-app/database"
	"product-app/docs"
	"product-app/router"
)

func main() {

	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Swagger Example API"
	docs.SwaggerInfo.Description = "This is a sample server Product APIs."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = ""
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	db := database.StartDB()
	controllerV1 := controllers.NewController(db)

	r := router.NewRouter(controllerV1)
	run := r.StartApp()
	run.Run(":8080")
}
