package helpers

import "net/http"

import (
	"errors"
	"fmt"
	"runtime"
	"strconv"
	"strings"
)

var ErrGeneral = 31

type ApplicationError struct {
	msg   string
	code  string
	cause error
}

func (e *ApplicationError) getErrorMessage() string {
	return e.msg
}

func (e *ApplicationError) Error() string {
	msg := e.getErrorMessage()
	if e.code != "" {
		msg = fmt.Sprintf("%s (type: %s)", msg, e.code)
	}
	if e.cause != nil {
		msg = fmt.Sprintf("%s: %v", msg, e.cause)
	}
	return msg
}

func (e *ApplicationError) getErrorCode() string {
	return e.code
}

func NewApplicationError(msg string, code string, cause error) error {
	applicationErr := &ApplicationError{
		msg:   msg,
		code:  code,
		cause: cause,
	}

	return applicationErr
}

func SetApplicationError(errMessage, errCode string) error {
	return NewApplicationError(errMessage, errCode, nil)
}

func SetApplicationErrorWithCause(errMessage, errCode string, cause error) error {
	return NewApplicationError(errMessage, errCode, cause)
}

// Error ...
func Error(err error, errCode int, errMessage string) error {

	if _, file, line, ok := runtime.Caller(1); ok {
		f := strings.Split(file, "/")
		errMessage = fmt.Sprintf("%s/%s:%d", errMessage, f[len(f)-1], line)
	}
	if err == nil {
		return SetApplicationError(errMessage, ErrorCodeString(errCode))
	}
	return SetApplicationErrorWithCause(errMessage, ErrorCodeString(errCode), err)
}

func ErrorCodeString(errCode int) string {
	return fmt.Sprintf("%d", errCode)
}

// ErrorMessage ...
func ErrorMessage(err error) string {
	return err.Error()
}

// ErrorMessage ...
func ErrorCode(err error) int {

	var applicationErr *ApplicationError

	if errors.As(err, &applicationErr) {
		code, _ := strconv.Atoi(applicationErr.getErrorCode())
		return code

	}

	return ErrGeneral
}

// ErrorCodeAndMessage ...
func ErrorCodeAndMessage(err error) (code int, remark string) {
	return ErrorCode(err), ErrorMessage(err)
}

var HTTP_STATUS_MAP = map[int]int{
	0:  http.StatusOK,           //ERR_SUCCESS
	1:  http.StatusOK,           //ERR_IN_PROGRESS
	2:  http.StatusCreated,      //ERR_PARAM_MISSING
	3:  http.StatusCreated,      //ERR_PARAM_MISSING
	11: http.StatusAccepted,     //ErrPaymentFailed
	10: http.StatusAccepted,     //ErrPaymentPending
	12: http.StatusAccepted,     //ErrPaymentExpired
	13: http.StatusAccepted,     //ErrPaymentInsufficient
	14: http.StatusOK,           //ErrPaymentSuccess
	19: http.StatusOK,           //ErrFulfillmentComplete
	20: http.StatusNotFound,     //ErrFulfillmentFailed
	21: http.StatusOK,           //ErrFulfillmentPartial
	22: http.StatusNotFound,     //ErrFulfillmentInProgress
	23: http.StatusGone,         //ErrFulfillmentTimeout
	24: http.StatusBadRequest,   //ErrDataNotFound
	25: http.StatusBadRequest,   //ErrDataAlreadyExists
	26: http.StatusBadRequest,   //ErrPackageNotFound
	27: http.StatusBadRequest,   //ErrPackageNotValid
	28: http.StatusBadRequest,   //ErrNoBillpayToPay
	29: http.StatusBadRequest,   // ErrGoodsNotValid
	30: http.StatusBadRequest,   //ErrRechargeFailed
	31: http.StatusBadRequest,   //ErrMdnCannotBuyPackage
	32: http.StatusBadRequest,   //ErrPackageIDNotFound
	33: http.StatusBadRequest,   //ErrRecharge
	34: http.StatusBadRequest,   //ErrEarningPoint
	35: http.StatusBadRequest,   //ErrBuyPackageFailed
	36: http.StatusBadRequest,   //ErrBuyPackageFailed
	37: http.StatusBadRequest,   //ErrBuyPackageFailed
	79: http.StatusBadRequest,   //ErrBlackListed
	80: http.StatusBadRequest,   //ErrGeneral
	81: http.StatusBadRequest,   //ErrInvalidFormat
	82: http.StatusNotFound,     //ErrClientNotFound
	83: http.StatusUnauthorized, //ErrTransactionNotAllowed
	84: http.StatusNotFound,     //ErrTransactionNotSupported
	85: http.StatusBadRequest,   //ErrDatabase
	86: http.StatusBadRequest,   //ErrMisingParamter
	90: http.StatusGone,         //ErrWorkflowCancelled
	91: http.StatusGone,         //ErrWorkflowTimeout
	92: http.StatusNotFound,     //ErrTimeout
}
