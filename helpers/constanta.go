package helpers

const (
	SuccessStatus  = 0
	FailedStatus   = 5
	SuccessMessage = "Success"
	FailedMessage  = "Failed"
)

const (
	DataCreated = 2
	DataUpdated = 3
)
